# Калькулятор
# import math
from colorama import init
from colorama import Fore, Back, Style
init()

print(Fore.BLACK)
print(Back.GREEN)
what = input(" Сложить (+) \n Вычесть (-) \n Умножить (*) \n Поделить (/) \n Чего изволите?: ")

print(Fore.BLACK)
print(Back.CYAN)
a = float(input("Введите значение: "))
b = float(input("Введите значение: "))

print(Fore.BLACK)
print(Back.RED)
if what == "+":
    c = a + b
    print("Результат: " + str(c))
elif what == "-":
    c = a - b
    print("Результат: " + str(c))
elif what == "*":
    c = a * b
    print("Результат: " + str(c))
elif what == "/":
    c = a / b
    print("Результат: " + str(c))
else:
    print("Неверная команда!")
